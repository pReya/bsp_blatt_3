/* Header file for vmappl.c
 * File: vmappl.h
 * Prof. Dr. Wolfgang Fohl, HAW Hamburg
 * 2010 
 */

#include "../vmem.h"

#ifndef VMACCESS_H
#define VMACCESS_H

#define AGING_CYCLE 20
#define VMEM_ALGO_FIFO  0
#define VMEM_ALGO_AGING 1
#define VMEM_ALGO_CLOCK 2

/** Connect to shared memory (key from vmem.h) */
void vm_init(void);

/** Read from "virtual" address */
int vmem_read(int address);

/** Write data to "virtual" address */
void vmem_write(int address, int data);

#endif
