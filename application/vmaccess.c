

#include "vmaccess.h"

#define REPORT_ERROR(cond,text) \
{ \
    if (cond == -1) { \
       printf("Fehler in Zeile: %d; Fehlercode : %d \n",text, cond ); \
       exit(1); \
    } \
}

struct vmem_struct *vmem = NULL;
sem_t *sema;

void aging(void);

/** Connect to shared memory (key from vmem.h) */
void vm_init(void)
{
	/* Allocate shared memory */
	int shm_id = shmget(SHMKEY,SHMSIZE,IPC_PRIVATE);
	REPORT_ERROR(shm_id, __LINE__);

	/* Attach shared memory */
    vmem = shmat(shm_id, 0, 0);
    
    sema = &(vmem->adm.sema);
}

/** Read from "virtual" address */
int vmem_read(int address)
{
    if(vmem == NULL)
    {
        vm_init();
    }

    int page_idx = address / VMEM_PAGESIZE;
    int offset = address % VMEM_PAGESIZE;
    REPORT_ERROR(sem_wait(sema),__LINE__);
    int page_flags = vmem->pt.entries[page_idx].flags;

    /* Not in RAM, need to fetch from pagefile */
    if((page_flags | PTF_PRESENT) != page_flags)
    {
        vmem->adm.req_pageno = page_idx;
        REPORT_ERROR(sem_post(sema),__LINE__);
        kill(vmem->adm.mmanage_pid, SIGUSR1);
        usleep(6000);
        REPORT_ERROR(sem_wait(sema),__LINE__);
    }

    int frame_idx = vmem->pt.entries[page_idx].frame;

    /* Read data */
    int result = vmem->data[(frame_idx*VMEM_PAGESIZE)+offset];

    /* Set Reference Flag to 1 */
    vmem->pt.entries[page_idx].flags = vmem->pt.entries[page_idx].flags | PTF_REF;

    /* Increment G counter */
    vmem->adm.g_count++;

    #if VMEM_ALGO == VMEM_ALGO_AGING
        aging();
    #endif

    REPORT_ERROR(sem_post(sema),__LINE__);

    return result;

}

/* Write data to "virtual" address */
void vmem_write(int address, int data)
{
    if(vmem == NULL)
    {
        vm_init();
    }

    int page_idx = address / VMEM_PAGESIZE;
    int offset = address % VMEM_PAGESIZE;
    REPORT_ERROR(sem_wait(sema),__LINE__);
    int page_flags = vmem->pt.entries[page_idx].flags;

    /* Not in RAM, need to fetch from pagefile */
    if((page_flags | PTF_PRESENT) != page_flags)
    {
        vmem->adm.req_pageno = page_idx;
        REPORT_ERROR(sem_post(sema),__LINE__);
        REPORT_ERROR(kill(vmem->adm.mmanage_pid, SIGUSR1),__LINE__);
        usleep(6000);
        REPORT_ERROR(sem_wait(sema),__LINE__);
    }

    int frame_idx = vmem->pt.entries[page_idx].frame;

    /* Write data */
    vmem->data[(frame_idx*VMEM_PAGESIZE)+offset] = data;

    /* Set Reference and Dirty Flag to 1 */
    vmem->pt.entries[page_idx].flags = vmem->pt.entries[page_idx].flags | PTF_REF;
    vmem->pt.entries[page_idx].flags = vmem->pt.entries[page_idx].flags | PTF_DIRTY;

    /* Increment G counter */
    vmem->adm.g_count++;

    #if VMEM_ALGO == VMEM_ALGO_AGING
        aging();
    #endif

    REPORT_ERROR(sem_post(sema),__LINE__);

}


void aging(void)
{

    if(vmem->adm.g_count%AGING_CYCLE == 0)
    {
        int i = 0;
        for(i = 0; i<VMEM_NFRAMES; i++)
        {   
            if(vmem->pt.framepage[i] != -1)
            {
                int page_idx = vmem->pt.framepage[i];        
                /* Isolate R bit */
                int r_bit = (vmem->pt.entries[page_idx].flags & PTF_REF) << 5;
                /* Shift to right (division by 2) */
                vmem->pt.entries[page_idx].count = vmem->pt.entries[page_idx].count >> 1;
                /* Add R bit to bit pos 7 (MSB) of count */
                vmem->pt.entries[page_idx].count = vmem->pt.entries[page_idx].count | r_bit;
                /* Reset Ref Bit */
                vmem->pt.entries[page_idx].flags = vmem->pt.entries[page_idx].flags & ~PTF_REF;
            }
        }
    }


}
