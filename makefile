
APPDIR = application
MANDIR = manage


all:
	cd $(APPDIR); \
	make VMEM_ALGO=$(VMEM_ALGO) VMEM_PAGESIZE=$(VMEM_PAGESIZE); \
	cd ../$(MANDIR); \
	make VMEM_ALGO=$(VMEM_ALGO) VMEM_PAGESIZE=$(VMEM_PAGESIZE); \
	cd ..;


clean:
	cd $(APPDIR); \
	make clean; \
	cd ../$(MANDIR); \
	make clean; \
	cd ..;