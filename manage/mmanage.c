/* Description: Memory Manager BSP3*/
/* Prof. Dr. Wolfgang Fohl, HAW Hamburg */
/* Sommer 2014
 * 
 * This is the memory manager process that
 * works together with the vmaccess process to
 * mimic virtual memory management.
 *
 * The memory manager process will be invoked
 * via a SIGUSR1 signal. It maintains the page table
 * and provides the data pages in shared memory
 *
 * This process is initiating the shared memory, so
 * it has to be started prior to the vmaccess process
 *
 * TODO:
 * currently nothing
 * */



#include "mmanage.h"

#define REPORT_ERROR(cond,text) \
{ \
    if (cond) { \
       printf("Fehler in Zeile: %d; Fehlercode : %d \n",text, cond ); \
       exit(1); \
    } \
}

#define PRINT_ERROR(text) \
{ \
	printf("Fehler in Zeile: %d\n",text); \
    exit(1); \
}


#define INT32MAXLEN 10
#define PF_LINE_LEN INT32MAXLEN+(INT32MAXLEN+1)*VMEM_PAGESIZE

struct vmem_struct *vmem = NULL;
FILE *pagefile = NULL;
FILE *logfile = NULL;
int signal_number = 0;          /* Received signal */
int vmem_algo = VMEM_ALGO_FIFO;
sem_t *sema;

int
main(void)
{
    struct sigaction sigact;

    /* Init pagefile */
    init_pagefile(MMANAGE_PFNAME);
    if(!pagefile) {
        perror("Error creating pagefile");
        exit(EXIT_FAILURE);
    }

    /* Open logfile */
    logfile = fopen(MMANAGE_LOGFNAME, "w");
    if(!logfile) {
        perror("Error creating logfile");
        exit(EXIT_FAILURE);
    }

    /* Create shared memory and init vmem structure */
    vmem_init();
    if(!vmem) {
        perror("Error initialising vmem");
        exit(EXIT_FAILURE);
    }
#ifdef DEBUG_MESSAGES
    else {
        fprintf(stderr, "vmem successfully created\n");
    }
#endif /* DEBUG_MESSAGES */

    /* Setup signal handler */
    /* Handler for USR1 */
    sigact.sa_handler = sighandler;
    sigemptyset(&sigact.sa_mask);
    sigact.sa_flags = 0;
    if(sigaction(SIGUSR1, &sigact, NULL) == -1) {
        perror("Error installing signal handler for USR1");
        exit(EXIT_FAILURE);
    }
#ifdef DEBUG_MESSAGES
    else {
        fprintf(stderr, "USR1 handler successfully installed\n");
    }
#endif /* DEBUG_MESSAGES */

    if(sigaction(SIGUSR2, &sigact, NULL) == -1) {
        perror("Error installing signal handler for USR2");
        exit(EXIT_FAILURE);
    }
#ifdef DEBUG_MESSAGES
    else {
        fprintf(stderr, "USR2 handler successfully installed\n");
    }
#endif /* DEBUG_MESSAGES */

    if(sigaction(SIGINT, &sigact, NULL) == -1) {
        perror("Error installing signal handler for INT");
        exit(EXIT_FAILURE);
    }
#ifdef DEBUG_MESSAGES
    else {
        fprintf(stderr, "INT handler successfully installed\n");
    }
#endif /* DEBUG_MESSAGES */

    /* Signal processing loop */
    while(1) {
        signal_number = 0;
        pause();
        if(signal_number == SIGUSR1) {  /* Page fault */
#ifdef DEBUG_MESSAGES
            fprintf(stderr, "Processed SIGUSR1\n");
#endif /* DEBUG_MESSAGES */
            signal_number = 0;
        }
        else if(signal_number == SIGUSR2) {     /* PT dump */
#ifdef DEBUG_MESSAGES
            fprintf(stderr, "Processed SIGUSR2\n");
#endif /* DEBUG_MESSAGES */
            signal_number = 0;
        }
        else if(signal_number == SIGINT) {
#ifdef DEBUG_MESSAGES
            fprintf(stderr, "Processed SIGINT\n");
#endif /* DEBUG_MESSAGES */
        }
    }

    return 0;
}

/* Your code goes here... */


void sighandler(int signo)
{
	signal_number = signo;
	switch (signo)
	{
	/* Page fault */
	case SIGUSR1:
		REPORT_ERROR(sem_wait(sema),__LINE__);
		allocate_page();
		REPORT_ERROR(sem_post(sema),__LINE__);

		break;
	/* Dump */
	case SIGUSR2:
		REPORT_ERROR(sem_wait(sema),__LINE__);
		dump_pt();
		REPORT_ERROR(sem_post(sema),__LINE__);
		break;
	/* User abort */
	case SIGINT:
		cleanup();
		exit(0);
		break;
	}
}

void init_pagefile(const char *pfname)
{
	pagefile = fopen(pfname, "w+");
	if (pagefile == NULL)
	{
		PRINT_ERROR(__LINE__)
	}

	/* Initialize random generator with seed */
	srand(SEED_PF);
	char new_line = '\n';

	int i,j;
	/* Pagefile lines, ex. 0-127 */
	for (i=0; i < VMEM_NPAGES; i++)
	{
		/* Pagefile line entries ex. 0-8 */
		for (j=0; j < VMEM_PAGESIZE; j++)
		{
			int randNum = rand();
			char stringFromInt[INT32MAXLEN];
			snprintf(stringFromInt,sizeof(stringFromInt),"%d",randNum);
			if (fwrite(stringFromInt,strlen(stringFromInt),1,pagefile) != 1)
				{
					PRINT_ERROR(__LINE__);
				}
            if (fwrite(",",sizeof(char),1,pagefile) != 1)
            	{
            		PRINT_ERROR(__LINE__);
            	}
		}
		if (fwrite(&new_line,sizeof(char),1,pagefile) != 1)
			{
				PRINT_ERROR(__LINE__);
			}
	}

}

void cleanup(void)
{
	/* Close all Streams */
	if(fclose(pagefile) != 0)
		{
			PRINT_ERROR(__LINE__);
		}

	/* Destroy Semaphor */
	REPORT_ERROR(sem_destroy(sema),__LINE__);

	/* Dettach Shared Memory from Process */
	REPORT_ERROR(shmdt(vmem),__LINE__);

#ifdef DEBUG_MESSAGES
            fprintf(stderr, "\nCleanup successful\n");
#endif /* DEBUG_MESSAGES */

}

void dump_pt(void)
{
	FILE *dumpFile = fopen(MMANAGE_DUMPFNAME, "w+");
	if (fwrite(vmem,sizeof(struct vmem_struct),1,dumpFile) != 1)
		{
			PRINT_ERROR(__LINE__);
		}
	if(fclose(dumpFile) == feof(stdin))
		{
			PRINT_ERROR(__LINE__);
		}
}

void vmem_init(void)
{
	int shm_id = shmget(SHMKEY,SHMSIZE,IPC_CREAT | 0777);
	vmem = shmat(shm_id, 0, 0);
    sema = &(vmem->adm.sema);
	vmem->adm.shm_id = shm_id;
	vmem->adm.g_count = 0;
	vmem->adm.mmanage_pid = getpid();
	vmem->adm.next_alloc_idx = 0;
	vmem->adm.pf_count = 0;
	vmem->adm.req_pageno = VOID_IDX;
	memset(vmem->adm.bitmap,0,(sizeof(VMEM_BMSIZE)*sizeof(vmem->adm.bitmap[0])));
	memset(vmem->pt.entries,0,(sizeof(struct pt_entry)*VMEM_NPAGES));
	vmem->adm.size = SHMSIZE;
	memset(vmem->data,VOID_IDX,sizeof(int) * VMEM_NFRAMES * VMEM_PAGESIZE);
    memset(vmem->pt.framepage,VOID_IDX,sizeof(int) * VMEM_NFRAMES);
	sem_init(sema, 1, 1);
}

void allocate_page(void)
{

    int frame_idx = search_bitmap();
    vmem->adm.pf_count++;

    struct logevent log;
    log.req_pageno = vmem->adm.req_pageno;
    log.pf_count = vmem->adm.pf_count;
    log.g_count = vmem->adm.g_count;


    if(frame_idx != -1)
    {
        log.alloc_frame = frame_idx;
    	log.replaced_page = -1;
    	logger(log);
        update_pt(frame_idx);
        fetch_page(vmem->adm.req_pageno);
        return;
    }

#ifndef VMEM_ALGO
    perror("Compiler Flag fuer Algo-Auswahl nicht gesetzt!");
    exit(-1);
#endif
#ifdef VMEM_ALGO
    switch (VMEM_ALGO)
    {
        case VMEM_ALGO_FIFO:
            frame_idx = find_remove_fifo();
            break;
        case VMEM_ALGO_CLOCK:
            frame_idx = find_remove_clock();
            break;
        case VMEM_ALGO_AGING:
            frame_idx = find_remove_aging();
            break;
        default:
            frame_idx = find_remove_fifo();
            break;
    }
#endif


    int old_page_idx = vmem->pt.framepage[frame_idx];
    int page_flags = vmem->pt.entries[old_page_idx].flags;

    if((page_flags | PTF_DIRTY) == page_flags)
    {
        store_page(old_page_idx);
    }

    /* Set all Flags to Zero */
    vmem->pt.entries[old_page_idx].flags = 0;

    /* Log Event */
    log.alloc_frame = frame_idx;
    log.replaced_page = old_page_idx;
    logger(log);

    update_pt(frame_idx);
    fetch_page(vmem->adm.req_pageno);

}

void update_pt(int frame)
{
	/* Update page table for requested page number with frame parameter */
	int pageNo = vmem->adm.req_pageno;

	vmem->pt.framepage[frame] = pageNo;
	vmem->pt.entries[pageNo].frame = frame;
	int arrayIndex = frame/VMEM_BITS_PER_BMWORD;

	/* Set Bitmap to 1 for frame */
	vmem->adm.bitmap[arrayIndex] = vmem->adm.bitmap[arrayIndex] | (1<<frame);

	/* Set present flag to 1 */
	vmem->pt.entries[pageNo].flags = PTF_PRESENT;
    vmem->pt.entries[pageNo].count = 0x80;
}

void fetch_page(int pt_idx)
{
	/* Buffer for one PT entry line */
	char buffer[PF_LINE_LEN];

	int i = 0;

    fseek(pagefile,0,SEEK_SET);
    for (i = 0; i <= pt_idx; i++)
    {

        if (fgets(buffer,PF_LINE_LEN,pagefile) == NULL)
        {
        	PRINT_ERROR(__LINE__);
        }
    }

	int lineData[VMEM_PAGESIZE];
	int j = 0;

	/* Read line data */
	for (i = 0; i < (sizeof(lineData)/sizeof(lineData[0])); i++)
	{
		int k = 0;
		char charToInt[INT32MAXLEN];
		while (buffer[j] != ',')
		{
			charToInt[k] = buffer[j];

			j++;
			k++;
		}
		j++;
		charToInt[k] = '\0';
		lineData[i] = atoi(charToInt);
	}

	/* Get pageframe for page index form page table */
	int pageframeIndex = vmem->pt.entries[pt_idx].frame;

	memcpy(&(vmem->data[pageframeIndex * VMEM_PAGESIZE]), lineData, sizeof(lineData));

}

void store_page(int pt_idx)
{
	int pageframeIndex = vmem->pt.entries[pt_idx].frame;
	int data[VMEM_PAGESIZE];
	char new_line = '\n';
	memcpy(&data, &(vmem->data[pageframeIndex * VMEM_PAGESIZE]), sizeof(data));
	fseek(pagefile,0,SEEK_SET);
	int i = 0;
    char not_used[PF_LINE_LEN] = {0};
    FILE *temp = fopen("pagefile.tmp","w+");

	/* Write to pagefile */
	for(i = 0; i < pt_idx; i++)
	{
		if (fgets(not_used,PF_LINE_LEN,pagefile) == NULL)
		{
			PRINT_ERROR(__LINE__);
		}
		if (fwrite(not_used,strlen(not_used),1,temp) != 1)
		{
			PRINT_ERROR(__LINE__);
		}
	}

	int j = 0;
	for(j = 0; j < sizeof(data)/sizeof(data[0]); j++)
	{
		char stringFromInt[INT32MAXLEN] = {0};
		snprintf(stringFromInt,sizeof(stringFromInt),"%d",data[j]);
		if (fwrite(stringFromInt,strlen(stringFromInt),1,temp) != 1)
		{
			PRINT_ERROR(__LINE__);
		}
		if (fwrite(",",sizeof(char),1,temp) != 1)
		{
			PRINT_ERROR(__LINE__);
		}
	}
	if (fwrite(&new_line,sizeof(char),1,temp) != 1)
	{
		PRINT_ERROR(__LINE__);
	}

	if (fgets(not_used,PF_LINE_LEN,pagefile) == NULL)
	{
		PRINT_ERROR(__LINE__);
	}

	while(1)
	{
		if (fgets(not_used,PF_LINE_LEN,pagefile) == NULL)
		{
			break;
		}
		if (fwrite(not_used,strlen(not_used),1,temp) != 1)
		{
			PRINT_ERROR(__LINE__);
		}
	}
	if(fclose(temp) != 0)
	{
		PRINT_ERROR(__LINE__);
	}

	if(fclose(pagefile) != 0)
	{
		PRINT_ERROR(__LINE__);
	}

	if (rename("pagefile.tmp","pagefile.bin") == -1)
	{
		perror("Fehler beim Schreiben und Umbenennen der Pagefile!");
	}
	pagefile = fopen(MMANAGE_PFNAME, "r+");
	if (pagefile == NULL)
	{
		PRINT_ERROR(__LINE__);
	}

}

int search_bitmap(void)
{
    Bmword bitMask_bm_full = 0;
    int i;
    if(VMEM_NFRAMES < VMEM_BITS_PER_BMWORD)
    {
        for(i = 0; i < VMEM_NFRAMES; i++)
        {
            bitMask_bm_full = bitMask_bm_full << 1;
            bitMask_bm_full = bitMask_bm_full | 1;
        }
    }
    else
    {
        bitMask_bm_full = UINT_MAX;        
    }


    char flag_frame_free = FALSE;
    for(i = 0; i<VMEM_BMSIZE; i++)
    {
        if(vmem->adm.bitmap[i] != bitMask_bm_full)
        {
            flag_frame_free = TRUE;
            break;
        }
    }

    if(flag_frame_free)
    {
        return find_free_bit(vmem->adm.bitmap[i], bitMask_bm_full);
    }

    return -1;

}

int find_free_bit(Bmword bmword, Bmword mask)
{
    int i = 0;
    while(mask != 0)
    {
        char last_bit_bmword = bmword & 1;
        char last_bit_mask = mask & 1;

        if(last_bit_mask != last_bit_bmword)
        {
            break;
        }

        bmword = bmword >> 1;
        i++;
    }
    return i;
}

int find_remove_fifo(void)
{
    int temp_frame_idx = vmem->adm.next_alloc_idx;
    vmem->adm.next_alloc_idx++;

    if(vmem->adm.next_alloc_idx == VMEM_NFRAMES)
    {
        vmem->adm.next_alloc_idx = 0;
    }

    return temp_frame_idx;
}


int find_remove_clock(void)
{
    if(vmem->adm.next_alloc_idx == VMEM_NFRAMES)
    {
        vmem->adm.next_alloc_idx = 0;
    }

    int temp_frame_idx = vmem->adm.next_alloc_idx;
    int temp_page_idx = vmem->pt.framepage[temp_frame_idx];
    char page_flags = vmem->pt.entries[temp_page_idx].flags;


    vmem->adm.next_alloc_idx++;
    if((page_flags | PTF_REF) == page_flags)
    {
        vmem->pt.entries[temp_page_idx].flags = vmem->pt.entries[temp_page_idx].flags & ~(PTF_REF);
        return find_remove_clock();
    }

    return temp_frame_idx;
}

int find_remove_aging(void)
{   
    int min = -1;
    int min_idx = -1;
    int i;
    for(i = 0; i<VMEM_NFRAMES; i++)
        {
            if(vmem->pt.framepage[i] != -1)
            {    
                int page_idx = vmem->pt.framepage[i];
                if(min == -1)
                {
                    min = vmem->pt.entries[page_idx].count;
                    min_idx = page_idx;
                }
                else if(vmem->pt.entries[page_idx].count <= min)
                {
                    min = vmem->pt.entries[page_idx].count;
                    min_idx = page_idx;
                }
            }
        }


	return vmem->pt.entries[min_idx].frame;
}


/* Do not change!  */
void
logger(struct logevent le)
{
	if (fprintf(logfile, "Page fault %10d, Global count %10d:\n"
            "Removed: %10d, Allocated: %10d, Frame: %10d\n",
            le.pf_count, le.g_count,
            le.replaced_page, le.req_pageno, le.alloc_frame) < 0)
		{
			PRINT_ERROR(__LINE__);
		}
    fflush(logfile);
}
